(*
	© Clément Joly 2017

   leo@wzukw.eu.org

	This software is a computer library whose purpose is to add a hook system in
	your program.

	This software is governed by the CeCILL-C license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/or redistribute the software under the terms of the CeCILL-C
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	As a counterpart to the access to the source code and  rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author,  the holder of the
	economic rights,  and the successive licensors  have only  limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL-C license and that you accept its terms.

*)

open Core.Std;;

module A = Alcotest;;
module Hk = Hook;;
module HT = Hook.Hook_table;;

(* Example of variant to be used as hook_point *)
type hk_points =
  | Pre_commit
  | Post_commit
  | Pre_push
;;

let ht_poly : (hk_points, string, bool) HT.t =
  new HT.t ()
;;

let ht_int =
  new HT.t
    ~hooks:[
      (1, [ ( + ); ( - ) ; ( * ) ])
    ; (3, [ ( + ); ( - ) ; ( * ) ])
    ]
    ~hashable:Int.hashable
    ()
;;

let not_registered () =
  A.check_raises "Hook point not registered" HT.No_registered_hook_point
    (fun () -> ht_int#trigger_hook_exn 0 1)
;;

let check_order () =
  let n = 10 in
  let ar = Array.init (n+1) ~f:(fun _ -> -1) in
  let ht = new HT.t
    ~hooks:[(0, List.init n
               ~f:(function
                     0 -> (fun _ -> ar.(0) <- 1)
                   | i -> (fun _ -> ar.(i) <- 2*ar.(i-1))
                 )
            )]
    ~hashable:Int.hashable
    ()
  in
  ht#register_hook_fun 0 [ (fun _ -> ar.(n) <- 2*ar.(n-1)) ];
  ht#trigger_hook_exn 0 1;
  A.(check ( array int ))
    "Check function are executed in the order they entered"
    ( Array.init (n+1) ~f:(Int.pow 2))
    ar
;;

let hooktbl_set =
  [
    "Raise exn if a non-registered hook point is triggered", `Quick, not_registered
  ; "Hook funtions are executed in order of “entrance”", `Quick, check_order
  ]
;;

let value = 10.;;
let add_content store_bckd =
  store_bckd#add "One" 1
    [ (fun i -> (Float.of_int i) +. value); (fun _ -> value) ];
  store_bckd#add "Two" 2 [ (fun i -> (Float.of_int i) +. value); (fun _ -> value) ];
  store_bckd#add "Three" 3 [ (fun i -> (Float.of_int i) +. value); (fun _ -> value) ];
  store_bckd#add "Four" 4 [ (fun i -> (Float.of_int i) +. value) ];
  store_bckd
;;

let all_store () =
  let s = Hk.Store_backend.all (Int.comparator) (String.hashable) in
  add_content s
;;

let get_some bckd () =
  let b = bckd () in
  A.(check ( option float ))
    "Get an element, in bound"
    (Some 14.)
    (b#get "Four" 4 0)
;;

let get_none bckd () =
  let b = bckd () in
  A.(check ( option float ))
    "Get an element, out of bound"
    (None)
    (b#get "Four" 4 1)
;;

let get_all bckd () =
  let b = bckd () in
  A.(check ( list float ))
    "Get all stored elements"
    [ 14. ]
    (b#get_all "Four" 4)
;;

let purge bckd () =
  let b = bckd () in
  b#purge;
  A.(check
       (list (pair
                string
                (list ( pair int (list float ) )))
       ))
    "Purge the backend"
    []
    b#to_alist

let storage_set =
  [
    "All#get (some)", `Quick, get_some all_store
  ; "All#get (none)", `Quick, get_none all_store
  ; "All#get_all", `Quick, get_all all_store
  ; "All#purge", `Quick, purge all_store
  ]

let () =
  Alcotest.run "Hook lib tests"
    [
      "Hook_table", hooktbl_set
    ; "Storage_backend", storage_set
    ]
