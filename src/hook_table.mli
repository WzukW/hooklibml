(*
	© Clément Joly 2017

   leo@wzukw.eu.org

	This software is a computer library whose purpose is to add a hook system in
	your program.

	This software is governed by the CeCILL-C license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/or redistribute the software under the terms of the CeCILL-C
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	As a counterpart to the access to the source code and  rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author,  the holder of the
	economic rights,  and the successive licensors  have only  limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL-C license and that you accept its terms.

*)

open Core.Std;;

(** A hook table gathers hooks with types in common *)

module H = Core.Std.Hashtbl
module Ph = Core.Std.Pooled_hashtbl.Hashable

(** Raised when you try to trigger a hook_point which was not properly
    registered. *)
exception No_registered_hook_point

(** Signature of a hook table

    'hook_point: Type of the identifier of a point where hook may be triggered ({i like
      pre-commit or pre-push for Git }).

      You may use any type, the only constraint is to be able to use it in an
      Hashtable. Strings, integers or variants are classical way to indentify
      hook points.

      Extendable variant types may be usefull here, to avoid the danger of
      unreferenced strings.

    'parameter: Type of the parameter passed to the hook function ({i argument(s) passed
    to a script like commit message with Git })

    'return_t: Type of values returned by hooks. Allows to define a hook_fun
    Hook_fun: Actual hook ({e it is a script with Git }), function performing some job
    on a hook point

    @param hooks: A set of hook, like this:
    [(hook_point1, [(fun param -> result);(fun param -> result)])
    ;(hook_point2, [(fun param -> result)])
    ]
    Note that hook points need to be different.

    @param hashable: Contains required functions for the hashtable, using
    polymorphic subsitute if not provided. Note that polymorphic hashing or
    comparison operator may not work reliably on large or deep data structure
    (@see
    <https://realworldocaml.org/v1/en/html/maps-and-hash-tables.html#the-polymorphic-comparator>,
    perils).

    @param store: A store backend, conditionning the way results may be stored (or not).
*)

class ['hook_point, 'parameter, 'return_t] t :
  ?hooks:('hook_point, ('parameter -> 'return_t) list) List.Assoc.t ->
  ?hashable:'hook_point Ph.t ->
  ?store:('hook_point, 'parameter, 'return_t) Store_backend.almost_a_backend ->
  unit ->
  object
    val hook_table : ('hook_point, ('parameter -> 'return_t) list) H.t

    (** Executes hook functions in the order they were entered for a given key *)
    method register_hook_fun :
      'hook_point -> ('parameter -> 'return_t) list -> unit

    (** Add hook functions at a given point *)
    method trigger_hook_exn : 'hook_point -> 'parameter -> unit

    (** Get storage backend, to retrieve results stored for instance *)
    method get_storage_backend
        : ('hook_point, 'parameter, 'return_t) Store_backend.t

  end
