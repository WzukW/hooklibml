(*
	© Clément Joly 2017

   leo@wzukw.eu.org

	This software is a computer library whose purpose is to add a hook system in
	your program.

	This software is governed by the CeCILL-C license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/or redistribute the software under the terms of the CeCILL-C
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	As a counterpart to the access to the source code and  rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author,  the holder of the
	economic rights,  and the successive licensors  have only  limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL-C license and that you accept its terms.

*)

open Core.Std;;

module Ph = Core.Std.Pooled_hashtbl.Hashable

(** Module gathering backends to store (or not!) results of executions of hook
    functions *)

(** Signature of a storage backend *)
class type ['hook_point, 'parameter, 'return_t] t =
  object
    (* Store something *)
    method add :
      'hook_point -> 'parameter -> ('parameter -> 'return_t) list -> unit
    (* get hk_pt param n : Get what the nth function at hook point hk_pt with
       parameter param returned *)
    method get : 'hook_point -> 'parameter -> int -> 'return_t Option.t
    (* Idem, but with all functions *)
    method get_all : 'hook_point -> 'parameter -> 'return_t List.t
    (* Remove all stored entry, to free memory for instance *)
    method purge : unit

    (* Get all data as an associated list, for instance to debug *)
    method to_alist : ( 'hook_point, ('parameter, 'return_t list) List.Assoc.t) List.Assoc.t

  end

(** Get a backend from the hash function used with hook points *)
type ('hook_point, 'parameter, 'return_t) almost_a_backend =
  'hook_point Ph.t ->
  ('hook_point, 'parameter, 'return_t) t
;;

(** Some backends *)

(** Stores none of the results *)
val nothing : ('hook_point, 'parameter, 'return_t) almost_a_backend

(** Stores last result but for all parameters, for all hook_point. The
    comparator is used to work efficiently with parameters *)
val all : ('parameter, 'b) Comparator.t ->
  ('hook_point, 'parameter, 'return_t) almost_a_backend
