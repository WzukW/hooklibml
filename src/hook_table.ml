(*
© Clément Joly 2017

   leo@wzukw.eu.org

This software is a computer library whose purpose is to add a hook system in
your program.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.

*)

open Core.Std;;

module H = Hashtbl
module Ph = Pooled_hashtbl.Hashable

exception No_registered_hook_point

class ['hook_point, 'parameter, 'return_t] t
    ?hooks
    ?hashable
    ?store
    ()
=
let hooks = Option.value ~default:[] hooks in
let hashable =
  Option.value ~default:Hashtbl.Poly.hashable hashable
in
let store : ('hook_point, 'parameter, 'return_t) Store_backend.t =
  Option.value ~default:(Store_backend.nothing) store
  |> fun almost_backend -> almost_backend hashable
in
object

  val hook_table : ('hook_point, ('parameter -> 'return_t) list) H.t =
    H.of_alist_exn
      hooks
      ~hashable

  method trigger_hook_exn hook_point param =
    let hook_funs =
      try H.find_exn hook_table hook_point
      with Not_found ->
        raise No_registered_hook_point
    in
    store#add hook_point param hook_funs

  method register_hook_fun key funs =
    H.change hook_table key
      ~f:(function
          | Some hook_fun_list -> Some (hook_fun_list @ funs)
          | None -> Some funs
        )

  method get_storage_backend
    : ('hook_point, 'parameter, 'return_t) Store_backend.t
    = store

end
;;

