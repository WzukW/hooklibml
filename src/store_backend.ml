(*
	© Clément Joly 2017

   leo@wzukw.eu.org

	This software is a computer library whose purpose is to add a hook system in
	your program.

	This software is governed by the CeCILL-C license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/or redistribute the software under the terms of the CeCILL-C
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	As a counterpart to the access to the source code and  rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author,  the holder of the
	economic rights,  and the successive licensors  have only  limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL-C license and that you accept its terms.

*)

open Core.Std;;

module Ph = Core.Std.Pooled_hashtbl.Hashable

class type ['hook_point, 'parameter, 'return_t] t =
  object
    method add : 'hook_point -> 'parameter -> ('parameter -> 'return_t) list -> unit
    method get : 'hook_point -> 'parameter -> int -> 'return_t Option.t
    method get_all : 'hook_point -> 'parameter -> 'return_t List.t
    method purge : unit

    method to_alist : ( 'hook_point, ('parameter, 'return_t list) List.Assoc.t) List.Assoc.t
  end
;;

type ('hook_point, 'parameter, 'return_t) almost_a_backend =
  'hook_point Ph.t ->
  ('hook_point, 'parameter, 'return_t) t
;;

let nothing =
  (fun _ -> object
    method add _ parameter hk_fun_list =
      List.iter hk_fun_list ~f:(fun hk_fun -> hk_fun parameter |> ignore)
    (* Nothing was stored *)
    method get _ _ _ = None
    method get_all _ _ = []
    method purge = ()

    method to_alist = []
  end
  )
;;

let all compare_parameter =
  (fun hashable -> object(self)
    val results = Hashtbl.create ~hashable ()
    val compare_parameter = compare_parameter

    method add hook_point parameter hk_fun_list =
      let result_list =
        List.map hk_fun_list ~f:(fun hk_fun -> hk_fun parameter)
      in
      let result_tree () =
        Map.Tree.of_alist_exn ~comparator:compare_parameter
          [ (parameter, result_list) ]
      in
      let erase_last_result_in tree =
        (* If there is already a result stored for the given parameter, erase it
           with the new result. Otherwise, add it to the tree. *)
        Map.Tree.change ~comparator:compare_parameter tree parameter
          ~f:(function None | Some _-> Some result_list)
      in
      Hashtbl.change results hook_point
        ~f:(function
              None -> Some (result_tree ())
            | Some tree -> Some (erase_last_result_in tree)
          )

    method get hook_point parameter fun_num =
      List.nth (self#get_all hook_point parameter) fun_num

    method get_all hook_point parameter =
      Hashtbl.find results hook_point
      |> Option.bind ~f:(fun tree ->
          Map.Tree.find tree parameter ~comparator:compare_parameter)
      |> function None -> [] | Some l -> l

    method purge =
      Hashtbl.filter_inplace results ~f:(fun _ -> false)

    method to_alist =
      Hashtbl.to_alist results
      |> List.map ~f:(fun (hk_point, map_tree) ->
          let alist = Map.Tree.to_alist map_tree in
          (hk_point, alist)
        )

  end
  )
;;
